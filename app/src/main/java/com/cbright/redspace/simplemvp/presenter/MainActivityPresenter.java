package com.cbright.redspace.simplemvp.presenter;

import com.cbright.redspace.simplemvp.contract.MainActivityContract;
import com.cbright.redspace.simplemvp.contract.MainActivityContract.Model;
import com.cbright.redspace.simplemvp.contract.MainActivityContract.View;
import com.cbright.redspace.simplemvp.model.MainActivityModel;

public class MainActivityPresenter implements MainActivityContract.Presenter {

    private View mView;
    private Model mModel;

    public MainActivityPresenter(View view) {
        mView = view;
        initPresenter();
    }

    private void initPresenter() {
        mModel = new MainActivityModel();
        mView.initView();
    }

    @Override
    public void onClick(android.view.View view) {
        String data = mModel.getData();
        mView.setViewData(data);
    }
}