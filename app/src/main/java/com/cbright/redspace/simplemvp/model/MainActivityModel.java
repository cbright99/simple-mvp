package com.cbright.redspace.simplemvp.model;

import com.cbright.redspace.simplemvp.contract.MainActivityContract;
import com.cbright.redspace.simplemvp.contract.MainActivityContract.Presenter;

public class MainActivityModel implements MainActivityContract.Model {

    @Override
    public String getData() {
        return "Hello World";
    }
}
